﻿using ScapySharp.Network.Packets;
using ScapySharp.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ScapySharp.Visual.Controls
{
    public abstract class PacketView : StackPanel 
    {


        protected List<FieldView> _Fields;
        protected List<StackPanel> _32BitStacker;
        protected RawPacket PacketData
        {
            get { return _PacketData; }
        }

        private RawPacket _PacketData;

        public PacketView(RawPacket packet)
        {
            this._PacketData = packet;
            

            _Fields = new List<FieldView>();
            _32BitStacker = new List<StackPanel>();
            this.LoadFields();
            
            _32BitStacker.Add(new StackPanel()
            {
                Orientation = Orientation.Horizontal
            });

            int bitCounter = 0;
            foreach (FieldView view in _Fields)
            {
                if (bitCounter >= 32)
                {
                    bitCounter = 0;
                    _32BitStacker.Add(new StackPanel()
                    {
                        Orientation = Orientation.Horizontal
                    });
                }

                _32BitStacker.Last().Children.Add(view);
                bitCounter += view.BitValue.Values.Length;
            }
            foreach (StackPanel p in _32BitStacker)
            {
                this.Children.Add(p);
            }

        }

        protected abstract void LoadFields();

        public List<FieldView> Fields
        {
            get { return _Fields; }
            set { _Fields = value; }
        }

    }
}
