﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScapySharp.Protocols;
using ScapySharp.Protocols.Ethernet;
using ScapySharp.Visual.Controls.Value_Parsers;

namespace ScapySharp.Visual.Controls.Packet_Viewers.Header_Viewers
{
    public class EthernetHeaderViewer : HeaderView
    {
        private static EthernetHeaderViewer singleton = new EthernetHeaderViewer();
        private EthernetHeaderViewer()
        {

        }

        public static EthernetHeaderViewer GetInstance()
        {
            return singleton;
        }
        public override void AddFields(List<FieldView> fields, Header header)
        {
            if (!(header is EthernetHeader))
                throw new Exception();

            EthernetHeader ethernetHeader = header as EthernetHeader;
            BitValueParser parser = new BitValueParser();
            fields.Add(new FieldView("Preamble", ethernetHeader.Preamble, parser));
            fields.Add(new FieldView("Delimeter", ethernetHeader.Delimeter, parser));
            fields.Add(new FieldView("MAC Source", ethernetHeader.MACSource, parser));
            fields.Add(new FieldView("MAC Destination", ethernetHeader.MACDestination, parser));
            fields.Add(new FieldView("Ethernet Type", ethernetHeader.Ethertype, parser));
            fields.Add(new FieldView("CRC", ethernetHeader.CRC, parser));
          //  fields.Add(new FieldView("Packet Gap", ethernetHeader.PacketGap, parser));
        }
    }
}
