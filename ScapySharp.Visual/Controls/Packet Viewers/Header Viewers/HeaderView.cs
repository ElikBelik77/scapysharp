﻿using ScapySharp.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Visual.Controls.Packet_Viewers.Header_Viewers
{
    public abstract class HeaderView 
    {

        public abstract void AddFields(List<FieldView> fields, Header header);

    }
}
