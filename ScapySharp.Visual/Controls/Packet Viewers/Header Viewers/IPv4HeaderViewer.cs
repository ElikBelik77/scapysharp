﻿using ScapySharp.Protocols;
using ScapySharp.Protocols.IP;
using ScapySharp.Visual.Controls.Value_Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Visual.Controls.Packet_Viewers.Header_Viewers
{
    public class IPv4HeaderViewer : HeaderView
    {
        private static IPv4HeaderViewer singleton = new IPv4HeaderViewer();
        private IPv4HeaderViewer()
        {

        }

        public static IPv4HeaderViewer GetInstance()
        {
            return singleton;
        }


        public override void AddFields(List<FieldView> fields,Header header)
        {
            if (!(header is IPv4Header))
                throw new Exception();
            BitValueParser bitValueParser = new BitValueParser();
            IPValueParser ipValueParser = new IPValueParser();
            IPv4Header ipHeader = header as IPv4Header;

            fields.Add(new FieldView("Version", ipHeader.Version, bitValueParser));
            fields.Add(new FieldView("Header Length", ipHeader.IHL, bitValueParser));
            fields.Add(new FieldView("Type of Service", ipHeader.TOS, bitValueParser));
            fields.Add(new FieldView("Total Length", ipHeader.TotalLength, bitValueParser));
            fields.Add(new FieldView("Identification", ipHeader.Identification, bitValueParser));
            fields.Add(new FieldView("Flags", ipHeader.Flags, bitValueParser));
            fields.Add(new FieldView("Fragmentation", ipHeader.FragmentOffset, bitValueParser));
            fields.Add(new FieldView("Time To Live", ipHeader.TimeToLive, bitValueParser));
            fields.Add(new FieldView("Protocol", ipHeader.Protocol, bitValueParser));
            fields.Add(new FieldView("Header Checksum", ipHeader.HeaderChecksum, bitValueParser));
            fields.Add(new FieldView("Source IP Address", ipHeader.SourceIPAddress, bitValueParser));
            fields.Add(new FieldView("Destination IP Address", ipHeader.DestinationIPAddress, bitValueParser));
        }
    }
}
