﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScapySharp.Protocols;
using ScapySharp.Visual.Controls.Value_Parsers;
using ScapySharp.Protocols.UDP;

namespace ScapySharp.Visual.Controls.Packet_Viewers.Header_Viewers
{
    public class UDPHeaderViewer : HeaderView
    {
        private static UDPHeaderViewer singleton = new UDPHeaderViewer();
        private UDPHeaderViewer()
        {

        }

        public static UDPHeaderViewer GetInstance()
        {
            return singleton;
        }

        public override void AddFields(List<FieldView> fields, Header header)
        {
            if (!(header is UDPHeader))
                throw new Exception();

            UDPHeader udpHeader = header as UDPHeader;
            BitValueParser parser = new BitValueParser();

            fields.Add(new FieldView("Source Port", udpHeader.SourcePort, parser));
            fields.Add(new FieldView("Destination Port", udpHeader.DestinationPort, parser));
            fields.Add(new FieldView("Length", udpHeader.Length, parser));
            fields.Add(new FieldView("CRC32", udpHeader.Checksum, parser));
        }
    }
}
