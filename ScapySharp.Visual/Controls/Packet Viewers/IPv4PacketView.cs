﻿using ScapySharp.Network.Packets;
using ScapySharp.Protocols.IP;
using ScapySharp.Visual.Controls.Packet_Viewers.Header_Viewers;
using ScapySharp.Visual.Controls.Value_Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ScapySharp.Visual.Controls.Protocol_Header_Viewers
{
    public class IPv4PacketView : PacketView
    {


        protected IPv4Packet _IPv4Packet
        {
            get { return (IPv4Packet)PacketData; }
        }

        public IPv4PacketView(IPv4Packet headerData) : base(headerData)
        {
        }

        protected override void LoadFields()
        {
            IPv4HeaderViewer ipHeaderViewer = IPv4HeaderViewer.GetInstance();
            ipHeaderViewer.AddFields(_Fields, _IPv4Packet.IPv4Header);
        }

    }
}
