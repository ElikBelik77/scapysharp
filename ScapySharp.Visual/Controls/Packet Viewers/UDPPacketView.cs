﻿using ScapySharp.Network.Packets;
using ScapySharp.Protocols.UDP;
using ScapySharp.Visual.Controls.Packet_Viewers.Header_Viewers;
using ScapySharp.Visual.Controls.Value_Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Visual.Controls.Protocol_Header_Viewers
{
    
    public class UDPPacketView : PacketView
    {
        //private UDPPacket _UDPPacket;

        public UDPPacket UDPPacket
        {
            get { return (UDPPacket)base.PacketData; }
            ///set { _UDPPacket = value; }
        }

        public UDPPacketView(UDPPacket packet) : base(packet)
        {
        }

        protected override void LoadFields()
        {
            IPv4HeaderViewer ipViewer = IPv4HeaderViewer.GetInstance();
            UDPHeaderViewer udpViewer = UDPHeaderViewer.GetInstance();
            EthernetHeaderViewer etherViewer = EthernetHeaderViewer.GetInstance();
            etherViewer.AddFields(_Fields, UDPPacket.EthernetHeader);
            ipViewer.AddFields(_Fields, UDPPacket.IPv4Header);
            udpViewer.AddFields(_Fields, UDPPacket.UDPHeader);
        }
    }
}
