﻿using ScapySharp.Utillities;
using ScapySharp.Visual.Controls.Value_Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ScapySharp.Visual.Controls
{
    public class FieldView : StackPanel
    {
        private MBit _BitValue;
        private string _FieldName;
        private Label _FieldNameLabel;
        private Label _FieldValueLabel;
        private ValueParser _Parser;

        public ValueParser Parser
        {
            get { return _Parser; }
            set { _Parser = value; }
        }

        public Label FieldValueLabel
        {
            get { return _FieldValueLabel; }
            set { _FieldValueLabel = value; }
        }

        public Label FieldNameLabel
        {
            get { return _FieldNameLabel; }
            set { _FieldNameLabel = value; }
        }

        public string Name
        {
            get { return _FieldName; }
            set { _FieldName = value; }
        }

        public MBit BitValue
        {
            get { return _BitValue; }
            set { _BitValue = value; }
        }

        public FieldView(string name, MBit bit, ValueParser parser)
        {
            _FieldName = name;
            _BitValue = bit;
            Orientation = Orientation.Vertical;
            _Parser = parser;
            _FieldNameLabel = new Label()
            {
                Content = _FieldName,
                FontSize = 7
            };
            _FieldValueLabel = new Label()
            {
                Content = _Parser.Parse(bit)
            };

            Children.Add(_FieldNameLabel);
            Children.Add(_FieldValueLabel);
            
        }
    }
}
