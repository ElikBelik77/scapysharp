﻿using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Visual.Controls.Value_Parsers
{
    public abstract class ValueParser
    {
        public abstract string Parse(MBit bit);
    }
}
