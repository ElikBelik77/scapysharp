﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScapySharp.Utillities;

namespace ScapySharp.Visual.Controls.Value_Parsers
{
    class BitValueParser : ValueParser
    {
        public override string Parse(MBit bit)
        {
            return String.Join("", bit.Values.Select(x => x ? 1 : 0));
        }
    }
}
