﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScapySharp.Utillities;

namespace ScapySharp.Visual.Controls.Value_Parsers
{
    public class IPValueParser : ValueParser
    {
        public override string Parse(MBit bit)
        {
            string ip = "";
            for(int i = 0; i < 4; i++)
            {
                ip += (int)bit[i*8, i * 8 + 8] + ".";
            }
            return ip.Substring(0, ip.Length - 1);
        }
    }
}
