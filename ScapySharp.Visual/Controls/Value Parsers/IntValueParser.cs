﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScapySharp.Utillities;

namespace ScapySharp.Visual.Controls.Value_Parsers
{
    public class IntValueParser : ValueParser
    {
        public override string Parse(MBit bit)
        {
            return ((int)bit).ToString();
        }
    }
}
