﻿using ScapySharp.Network.Packets;
using ScapySharp.Protocols;
using ScapySharp.Protocols.Ethernet;
using ScapySharp.Protocols.IP;
using ScapySharp.Protocols.UDP;
using ScapySharp.Utillities;
using ScapySharp.Visual.Controls.Protocol_Header_Viewers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScapySharp.Visual
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var macAddr =
    (
        from nic in NetworkInterface.GetAllNetworkInterfaces()
        where nic.OperationalStatus == OperationalStatus.Up
        select nic.GetPhysicalAddress().ToString()
    ).FirstOrDefault();
            EthernetHeader etherHeader = new EthernetHeader()
            {
                MACSource = IPUtillities.ConvertMACToMBit(macAddr),
                MACDestination = IPUtillities.ConvertMACToMBit(macAddr),//IPUtillities.ConvertIPToMBit(""),
                Ethertype = EthernetType.EthernetII,
            };
            IPv4Header ipv4Header = new IPv4Header()
            {
                TOS = DSCP.CreateDS(2, 3).PostConcat(ECN.ECT0()),
                Flags = Fragmentation.DF,
                TimeToLive = 50,
                Protocol = IPProtocols.UDP,
                SourceIPAddress = IPUtillities.ConvertIPToMBit("192.168.1.22"),
                DestinationIPAddress = IPUtillities.ConvertIPToMBit("192.168.1.22"),
                IHL = 5,
            };



            TCPHeader tcpHeader = new TCPHeader()
            {
                SourcePort = 8000,
                DestinationPort = 8000,
                DataOffset = 5,
                SYN = 1,
            };

            UDPHeader udpHeader = new UDPHeader()
            {
                SourcePort = 8000,
                DestinationPort = 8000,
                Length = 8,
            };


            //TCPPacket packet = new TCPPacket(ipv4Header, tcpHeader
            UDPPacket packet = new UDPPacket(etherHeader, ipv4Header, udpHeader, "hello world");
            UDPPacketView packetView = new UDPPacketView(packet);
            ViewGrid.Children.Add(packetView);

        }
    }
}
