﻿using ScapySharp.Protocols.Ethernet;
using ScapySharp.Protocols.IP;
using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Network.Packets
{
    public class IPv4Packet : RawPacket
    {
        private Payload _Payload;

        public Payload Payload
        {
            get { return _Payload; }
            set { _Payload = value; }
        }

        private EthernetHeader _EthernetHeader;
        private IPv4Header _IPv4Header;

        public IPv4Header IPv4Header
        {
            get { return _IPv4Header; }
            set { _IPv4Header = value; }
        }

        public EthernetHeader EthernetHeader
        {
            get { return _EthernetHeader; }
            set { _EthernetHeader = value; }
        }

        public IPv4Packet(EthernetHeader ether, IPv4Header ipv4Header, string payload)
        {
            
            
            _Payload = new Payload(payload);
            ether.SetPayloadSize((int)ipv4Header.TotalLength * 32 + _Payload.Data.Length * 8);
            ether.AddHeader(ipv4Header);

            ipv4Header.TotalLength = ipv4Header.HeaderLength * 4 + _Payload.Data.Length;
            ether.CalculateFCS();
            ipv4Header.CalculateChecksum();

            this._Header = ether;
            this._EthernetHeader = ether;
            this._IPv4Header = ipv4Header;
        }

        public override byte[] GetBytes()
        {
            byte[] etherBytes = ByteCompressor.Compress(_EthernetHeader.Preamble.PostConcat(_EthernetHeader.Delimeter).PostConcat(_EthernetHeader.MACSource).PostConcat(_EthernetHeader.MACDestination).PostConcat(_EthernetHeader.Ethertype).GetBytes());
            List<byte> bytes = etherBytes.ToList();
            bytes.AddRange(_IPv4Header.GetBytes());
            bytes.AddRange(_Payload.Data);
         //   bytes.AddRange(_UDPHeader.GetBytes());
            return bytes.ToArray();
        }
    }
}
