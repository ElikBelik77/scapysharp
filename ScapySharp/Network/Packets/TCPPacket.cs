﻿using ScapySharp.Protocols;
using ScapySharp.Protocols.Ethernet;
using ScapySharp.Protocols.IP;
using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Network.Packets
{
    public class TCPPacket : RawPacket
    {
        public TCPPacket(EthernetHeader etherHeader, IPv4Header ipv4Header, TCPHeader tcpHeader)
        {
            
            etherHeader.AddHeader(ipv4Header);
            etherHeader.AddHeader(tcpHeader);
            etherHeader.SetPayloadSize((ipv4Header.HeaderLength + tcpHeader.HeaderLength) * 32);
            _Header = etherHeader;
            etherHeader.CalculateFCS();
        }
    }
}
