﻿using ScapySharp.Protocols.Ethernet;
using ScapySharp.Protocols.IP;
using ScapySharp.Protocols.UDP;
using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Network.Packets
{
    public class UDPPacket : RawPacket
    {
        private EthernetHeader _EthernetHeader;
        private IPv4Header _IPV4Header;
        private UDPHeader _UDPHeader;
        private Payload _Payload;

        public EthernetHeader EthernetHeader
        {
            get { return _EthernetHeader; }
        }

        public IPv4Header IPv4Header
        {
            get { return _IPV4Header; }
        }

        public UDPHeader UDPHeader
        {
            get { return _UDPHeader; }
        }

     
        public Payload Payload
        {
            get { return _Payload; }
            set { _Payload = value; }
        }

        public UDPPacket(EthernetHeader etherHeader, IPv4Header ipv4Header, UDPHeader udpHeader, string payload)
        {
            _Payload = new Payload(payload);
            etherHeader.AddHeader(ipv4Header);
            etherHeader.AddHeader(udpHeader);
            etherHeader.SetPayloadSize((ipv4Header.HeaderLength) * 32 + (int)udpHeader.Length * 8 + _Payload.Data.Length * 8);
            ipv4Header.TotalLength = ipv4Header.HeaderLength * 4 + 8 + _Payload.Data.Length;
            udpHeader.Length = 8 + _Payload.Data.Length;
            _Header = etherHeader;

            ipv4Header.CalculateChecksum();
            etherHeader.CalculateFCS();
            udpHeader.CalculateCRC(ipv4Header, _Payload.Data);
            
            _EthernetHeader = etherHeader;
            _IPV4Header = ipv4Header;
            _UDPHeader = udpHeader;
        }

        public override byte[] GetBytes()
        {
            byte[] etherBytes =ByteCompressor.Compress(_EthernetHeader.Preamble.PostConcat(_EthernetHeader.Delimeter).PostConcat(_EthernetHeader.MACSource).PostConcat(_EthernetHeader.MACDestination).PostConcat(_EthernetHeader.Ethertype).GetBytes());
            List<byte> bytes = etherBytes.ToList();
            bytes.AddRange(_IPV4Header.GetBytes());
            bytes.AddRange(_UDPHeader.GetBytes());
            bytes.AddRange(_Payload.Data);
            return bytes.ToArray();
        }
    }
}
