﻿using ScapySharp.Protocols;
using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Network.Packets
{
    public class RawPacket
    {
        protected Header _Header;
        protected string _Payload;

        public string Payload
        {
            get { return _Payload; }
            set { _Payload = value; }
        }

        public Header Header
        {
            get { return _Header; }
            set { _Header = value; }
        }

        public RawPacket()
        {
        }

        public virtual byte[] GetBytes()
        {
            List<byte> bytes = new List<byte>();
            foreach (Header h in _Header)
            {
                bytes.AddRange(h.GetBytes());
            }
            return bytes.ToArray();
        }


        public Header FindHeader(string protocolName)
        {
            return _Header[protocolName];
        }


        public string HexDump()
        {
            return BitConverter.ToString(GetBytes()).Replace('-',' ');
        }
    }
}
