﻿using ScapySharp.Network.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Network
{
    public static class Network
    {

        private static Socket _sock;

        public static void Initialize()
        {
            _sock = new Socket(SocketType.Raw, ProtocolType.Raw);
            _sock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.HeaderIncluded, 1);
            //_sock.SetSocketOption(SocketOptionLevel.Udp, SocketOptionName.HeaderIncluded, 1);
            _sock.Bind(new IPEndPoint(IPAddress.Parse("192.168.1.22"),8000));
        }

        public static void Send(RawPacket p)
        {
            for (int i = 0; i < 10; i++)
            {
                _sock.SendTo(p.GetBytes(), new IPEndPoint(IPAddress.Parse("192.168.1.22"),8000));
            }
        }
    }
}
