﻿using ScapySharp.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Network.Packets
{
    public class Payload
    {
        private byte[] _Data;

        public byte[] Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        public Payload(string payload)
        {
            _Data = Encoding.ASCII.GetBytes(payload);
        }
    }
}
