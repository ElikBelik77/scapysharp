﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Utillities
{
    public static class ByteCompressor
    {

        public static byte[] Compress(byte[] looseBytes)
        {
            List<byte> bytes = new List<byte>();
            byte currentByte = 0;
            int bitCounter = 0;
            int rolCounter = 7;
            for (int i = 0; i < looseBytes.Length; i++, bitCounter++, bitCounter %= 8)
            {
                currentByte = (byte)(currentByte | (byte)((looseBytes[i] << rolCounter) | (looseBytes[i] >> (8 - rolCounter))));
                rolCounter--;
                if ((i + 1) % 8 != 0) 
                    continue;
                bytes.Add(currentByte);
                rolCounter = 7;
                currentByte = 0;
            }
            return bytes.ToArray<byte>();
        }

        public static byte ReverseByte(byte b)
        {
            return (byte)(((b * 0x0802u & 0x22110u) | (b * 0x8020u & 0x88440u)) * 0x10101u >> 16);
        }
    }
}
