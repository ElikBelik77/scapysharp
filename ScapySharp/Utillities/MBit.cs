﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Utillities
{
    public class MBit
    {

        private bool[] _Values;

        public MBit(int size)
        {
            _Values = new bool[size];
        }

        public MBit(MBit other)
        {
            _Values = new bool[other._Values.Length];
            Array.Copy(other._Values, _Values, _Values.Length);
        }

        public MBit(bool[] values)
        {
            _Values = values;
        }

        public MBit(int size, int value)
        {
            _Values = new bool[size];
            if (value > 0 && size < (int)Math.Log(2, value) + 1)
                throw new Exception("size too small for the given value");

            if (value == 0)
                return;
            MBit b = value;
            Array.Copy(b._Values, 0, _Values, size - b._Values.Length, b._Values.Length);
        }

        public bool this[int index]
        {
            get
            {
                return _Values[index];
            }
            set
            {
                if (index >= _Values.Length)
                {
                    throw new IndexOutOfRangeException();
                }
                _Values[index] = value;
            }
        }


        public MBit this[int start, int end]
        {
            get
            {
                bool[] newValues = new bool[end - start];
                for (int i = start; i < end; i++)
                    newValues[i-start] = Values[i];
                return new MBit(newValues);
            }
        }

        public bool[] Values
        {
            get { return _Values; }
        }

        public static implicit operator MBit(int value)
        {
            if(value == 0)
            {
                return new MBit (new bool[] { });
            }
            MBit bit = new MBit((int)Math.Log(value, 2) + 1);
            int index = bit._Values.Length - 1;
            while(value != 0)
            {
                bit[bit._Values.Length - 1 - index] = Math.Pow(2, index) <= value;
                value = Math.Pow(2, index) <= value ? value - (int)Math.Pow(2,index) : value;
                index--;
            }
            return bit;
        }
        
        public static explicit operator int(MBit value)
        {
            int sum = 0;
            for(int i = 0; i < value.Values.Length; i++)
            {
                if (!value.Values[i]) continue;
                sum += (int)Math.Pow(2, value.Values.Length - 1 - i);
            }
            return sum;
        }


        public MBit BitwiseNot()
        {
            MBit newBit = new MBit(this._Values.Length);
            for(int i = 0; i < newBit._Values.Length; i++)
            {
                newBit.Values[i] = !this.Values[i];
            }
            return newBit;
        }

    public MBit PreConcat(MBit other)
        {
            bool[] newValues = new bool[_Values.Length + other._Values.Length];
            newValues = other._Values.Concat(_Values).ToArray<bool>();
            return new MBit(newValues);
        }

        public MBit PostConcat(MBit other)
        {
            return other.PreConcat(this);
        }

        public byte[] GetBytes()
        {
            return Array.ConvertAll(_Values, x => x ? (byte)1 : (byte)0);
        }

        public void SetValue(MBit bit)
        {
            Array.Clear(_Values, 0, _Values.Length);
            Array.Copy(bit._Values, 0, _Values, _Values.Length - bit._Values.Length, bit._Values.Length);
        }


        public void SetSize(int size)
        {
            this._Values = new bool[size];
        }
    }
}
