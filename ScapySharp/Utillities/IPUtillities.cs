﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Utillities
{
    public static class IPUtillities
    {

        public static MBit ConvertIPToMBit(string ip)
        {
            string[] ipData = ip.Split('.');
            MBit result = new MBit(0);
            for(int i = 0; i < ipData.Length; i++)
            {
                result = result.PostConcat(new MBit(8,int.Parse(ipData[i])));
            }
            return new MBit(result.Values.ToArray<bool>());
        }

        public static MBit ConvertMACToMBit(string mac)
        {
            MBit result = new MBit(0);
            bool skip = false;
            foreach(Tuple<char,char> t in mac.Zip(mac.Skip(1), (a, b) => Tuple.Create(a, b)))
            {
                if(!skip)
                {
                    skip = true;
                }
                else
                {
                    skip = false;
                    continue;
                }
                int value = Convert.ToInt16(t.Item1.ToString(), 16);
                value *= 16;
                value += Convert.ToInt16(t.Item2.ToString(), 16);
                MBit bitValue = new MBit(8, value);
                result = result.PostConcat(bitValue);
            }

            return result;
        }

        public static int CalculateChecksum(byte[] data)
        {
            long longSum = data.Sum(x => (long)x);
            return (Int16)longSum;
        }
    }
}
