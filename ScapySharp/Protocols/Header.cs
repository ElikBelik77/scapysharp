﻿using ScapySharp.Utillities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols
{
    public abstract class Header : IEnumerable<Header>
    {
        private Header _NextHeader;
        protected List<MBit> _Bits;
        public Header NextHeader
        {
            get { return _NextHeader; }
            set { _NextHeader = value; }
        }

        public List<MBit> Fields
        {
            get { return _Bits; }
        }


        public Header()
        {
            _Bits = new List<MBit>();
        }


        public void ChainHeader(Header d)
        {
            if (IsCompactible(d))
            {
                if(_NextHeader != null)
                {
                    d._NextHeader = NextHeader;
                    _NextHeader = d;
                }
            }
            else
            {
                throw new Exception("incompactible header");
            }
        }

        public abstract bool IsCompactible(Header d);
        protected virtual byte[] GetHeaderData()
        {
            return _Bits.Aggregate((y, x) => x.PreConcat(y)).GetBytes();
        }

        public byte[] GetBytes()
        {
            byte[] bytes = GetHeaderData();
            return ByteCompressor.Compress(bytes);
        }

        public IEnumerator<Header> GetEnumerator()
        {
            return new HeaderEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new HeaderEnumerator(this);
        }

        public abstract Header this[string name]
        {
            get;
        }

        public Header this[int index]
        {
            get
            {
                if (index == 0)
                    return this;
                return _NextHeader[index--];
            }
        }

        class HeaderEnumerator : IEnumerator<Header>
        {
            private Header _Header;
            private Header _ParentHeader;
            private bool first = true;
            public Header Current => _Header;

            object IEnumerator.Current => Current;

            public HeaderEnumerator(Header h)
            {
                _Header = h;
                _ParentHeader = h;
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (!first)
                {
                    _Header = _Header.NextHeader;
                }
                else
                {
                    first = false;
                }

                return _Header != null; 
            }

            public void Reset()
            {
                _Header = _ParentHeader;
            }

        }
    }
}
