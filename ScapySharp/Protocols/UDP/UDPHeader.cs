﻿using ScapySharp.Protocols.IP;
using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.UDP
{
    public class UDPHeader : Header
    {



        private MBit _SourcePort;
        private MBit _DestinationPort;
        private MBit _Length;
        private MBit _Checksum;

        public UDPHeader() : base()
        {
            _SourcePort = new MBit(16, 0); _Bits.Add(_SourcePort);
            _DestinationPort = new MBit(16, 0); _Bits.Add(_DestinationPort);
            _Length = new MBit(16, 0); _Bits.Add(_Length);
            _Checksum = new MBit(16, 0); _Bits.Add(_Checksum);
        }


        public void CalculateCRC(IPv4Header ipHeader, byte[] payload)
        {
            
            List<byte> workingBytes = new List<byte>();
            workingBytes.AddRange(ByteCompressor.Compress(ipHeader.SourceIPAddress.PostConcat(ipHeader.DestinationIPAddress).PostConcat(ipHeader.Protocol.PreConcat(new MBit(8, 0))).PostConcat(this.Length).PostConcat(SourcePort).PostConcat(DestinationPort).PostConcat(new MBit(16,8 + payload.Length)).GetBytes()));
            workingBytes.AddRange(payload);
            if(payload.Length % 2 == 1)
            {
                workingBytes.Add(0);
            }
            bool skip = false;
            int sum = 0;
            foreach (Tuple<byte, byte> c in workingBytes.Zip(workingBytes.Skip(1), (a, b) => new Tuple<byte, byte>(a, b)))
            {
                if (!skip)
                {
                    skip = true;
                }
                else
                {
                    skip = false;
                    continue;
                }
                sum += (c.Item1 << 8) + c.Item2;
            }
            byte[] intBytes = BitConverter.GetBytes(sum);
            int fifthByte = (intBytes[2] << 4) >> 4;
            sum += fifthByte;
            sum %= (int)Math.Pow(2, 16);
            //Array.Reverse(sum);
            //int carry = (sum % (int)Math.Pow(2, 23)) / (int)Math.Pow(2, 15);
            //sum %= (int)Math.Pow(2, 15);
            //int checksum = carry + sum;
            this.Checksum = new MBit(16, sum).BitwiseNot();
        }

        public MBit Checksum
        {
            get { return _Checksum; }
            set { _Checksum.SetValue(value); }
        }

        public MBit Length
        {
            get { return _Length; }
            set { _Length.SetValue(value); }
        }

        public MBit DestinationPort
        {
            get { return _DestinationPort; }
            set { _DestinationPort.SetValue(value); }
        }

        public MBit SourcePort
        {
            get { return _SourcePort; }
            set { _SourcePort.SetValue(value); }
        }



        public override Header this[string name]
        {
            get
            {
                if("udp" == name.ToLower())
                {
                    return this;
                }
                return null;
            }
        }

        public override bool IsCompactible(Header d)
        {
            return true;
        }
    }
}
