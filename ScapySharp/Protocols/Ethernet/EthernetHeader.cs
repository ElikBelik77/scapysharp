﻿using ScapySharp.Network.Packets;
using ScapySharp.Utillities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.Ethernet
{
    public class EthernetHeader : Header
    {
        private MBit _Preamble;
        private MBit _Delimeter;
        private MBit _MACDestination;
        private MBit _MACSource;
        private MBit _Q802Tag;
        private MBit _Ethertype;
        private MBit _Payload;
        private MBit _CRC;
        private MBit _PacketGap;

        public MBit PacketGap
        {
            get { return _PacketGap; }
            set { _PacketGap.SetValue(value); }
        }

        public MBit CRC
        {
            get { return _CRC; }
            set { _CRC.SetValue(value); }
        }

        public MBit Payload
        {
            get { return _Payload; }
            set
            {
                _Payload.SetSize(value.Values.Length);
                _Payload.SetValue(value);
            }
        }

        public MBit Ethertype
        {
            get { return _Ethertype; }
            set { _Ethertype.SetValue(value); }
        }

        public MBit Q802Tag
        {
            get { return _Q802Tag; }
            set { _Q802Tag.SetValue(value); }
        }


        public MBit MACSource
        {
            get { return _MACSource; }
            set { _MACSource.SetValue(value); }
        }

        public MBit MACDestination
        {
            get { return _MACDestination; }
            set { _MACDestination.SetValue(value); }
        }

        public MBit Delimeter
        {
            get { return _Delimeter; }
            set { _Delimeter.SetValue(value); }
        }

        public MBit Preamble
        {
            get { return _Preamble; }
            set { _Preamble.SetValue(value);  }
        }

        public override Header this[string name]
        {
            get
            {
                if(name.ToLower() == "ethernet")
                {
                    return this;
                }
                return null;
            }
        }

        public EthernetHeader() : base()
        {
            _Preamble = new MBit(7 * 8); _Bits.Add(_Preamble);
            _Delimeter = new MBit(8); _Bits.Add(_Delimeter);
            _MACDestination = new MBit(6 * 8); _Bits.Add(_MACDestination);
            _MACSource = new MBit(6 * 8); _Bits.Add(_MACSource);
            //_Q802Tag = new MBit(4 * 8); _Bits.Add(_Q802Tag);
            _Ethertype = new MBit(2 * 8); _Bits.Add(_Ethertype);
            _Payload = new MBit(0); _Bits.Add(_Payload);
            _CRC = new MBit(4 * 8); _Bits.Add(_CRC);
            _PacketGap = new MBit(12 * 8); _Bits.Add(_PacketGap);
            
            for(int i = 0; i < _Preamble.Values.Length; i++)
            {
                _Preamble[i] = (i + 1) % 2 == 1;
            }

        }

        public override bool IsCompactible(Header d)
        {
            return true;
        }

        public void SetPayloadSize(int size)
        {
            if (size < Payload.Values.Length)
            {
                bool[] oldvalues = Payload.Values;
                Payload.SetSize(size);
                Array.Copy(oldvalues, Payload.Values, Payload.Values.Length);
            }
            else
            {
                _Payload = _Payload.PostConcat(new MBit(size - _Payload.Values.Length, 0));
            }
        }

        public void CalculateFCS()
        {
            CRC32 crc = new CRC32();
            int crcValue = (int)crc.ComputeChecksum(GetBytesForFCSCalculation());
            byte[] byteArray = BitConverter.GetBytes(crcValue);
            for(int i = 0; i < byteArray.Length; i++)
            {
                byteArray[i] = ByteCompressor.ReverseByte(byteArray[i]);
            }
            BitArray b = new BitArray(byteArray);
            for (int i = 0; i < b.Length; i++)
            {
                _CRC.Values[i] = b[i];
            }


        }

        public void AddHeader(Header h)
        {
            foreach(MBit b in h.Fields)
            {
                Payload = Payload.PostConcat(b);
            }
        }

        private byte[] GetBytesForFCSCalculation()
        {
            List<byte> result = new List<byte>();
            foreach (MBit b in _Bits)
            {
                if (b == CRC || b == _Preamble || b == Delimeter || b == PacketGap)
                    continue;
                result.AddRange(b.GetBytes());
            }
            return ByteCompressor.Compress(result.ToArray());
        }
    }
}
