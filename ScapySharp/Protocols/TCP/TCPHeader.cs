﻿using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
    
namespace ScapySharp.Protocols
{
    public class TCPHeader : Header
    {

        private MBit _SourcePort;
        private MBit _DestinationPort;
        private MBit _SequenceNumber;
        private MBit _AcknowledgementNumber;
        private MBit _DataOffset;
        private MBit _Reserved;
        private MBit _NS;
        private MBit _CWR;
        private MBit _ECE;
        private MBit _URG;
        private MBit _ACK;
        private MBit _PSH;
        private MBit _RST;
        private MBit _SYN;
        private MBit _FIN;
        private MBit _WindowSize;
        private MBit _Checksum;
        private MBit _UrgentPointer;
        private MBit _Options;

        public TCPHeader()
        {
            _SourcePort = new MBit(16, 0); _Bits.Add(_SourcePort);
            _DestinationPort = new MBit(16, 0); _Bits.Add(_DestinationPort);
            _SequenceNumber = new MBit(32, 0); _Bits.Add(_SequenceNumber);
            _AcknowledgementNumber = new MBit(32, 0); _Bits.Add(_AcknowledgementNumber);
            _DataOffset = new MBit(4, 0); _Bits.Add(_DataOffset);
            _Reserved = new MBit(3, 0); _Bits.Add(_Reserved);
            _NS = new MBit(1, 0); _Bits.Add(_NS);
            _CWR = new MBit(1, 0); _Bits.Add(_CWR);
            _ECE = new MBit(1, 0); _Bits.Add(_ECE);
            _URG = new MBit(1, 0); _Bits.Add(_URG);
            _ACK = new MBit(1, 0); _Bits.Add(_ACK);
            _PSH = new MBit(1, 0); _Bits.Add(_PSH);
            _RST = new MBit(1, 0); _Bits.Add(_RST);
            _SYN = new MBit(1, 0); _Bits.Add(_SYN);
            _FIN = new MBit(1, 0); _Bits.Add(_FIN);
            _WindowSize = new MBit(16, 0); _Bits.Add(_WindowSize);
            _Checksum = new MBit(16, 0); _Bits.Add(_Checksum);
            _UrgentPointer = new MBit(16, 0); _Bits.Add(_UrgentPointer);
            _Options = new MBit(0, 0); _Bits.Add(_Options);
        }

        public int HeaderLength
        {
            get { return 5 + Options.Values.Length/8; }
        }

        public MBit Options
        {
            get { return _Options; }
            set { _Options = value; }
        }

        public MBit UrgentPointer
        {
            get { return _UrgentPointer; }
            set { _UrgentPointer = value; }
        }

        public MBit Checksum
        {
            get { return _Checksum; }
            set { _Checksum = value; }
        }

        public MBit WindowSize
        {
            get { return _WindowSize; }
            set { _WindowSize = value; }
        }

        public MBit FIN
        {
            get { return _FIN; }
            set { _FIN = value; }
        }

        public MBit SYN
        {
            get { return _SYN; }
            set { _SYN = value; }
        }

        public MBit RST
        {
            get { return _RST; }
            set { _RST = value; }
        }

        public MBit PSH
        {
            get { return _PSH; }
            set { _PSH = value; }
        }

        public MBit ACK
        {
            get { return _ACK; }
            set { _ACK = value; }
        }

        public MBit URG
        {
            get { return _URG; }
            set { _URG = value; }
        }

        public MBit ECE
        {
            get { return _ECE; }
            set { _ECE = value; }
        }

        public MBit CWR
        {
            get { return _CWR; }
            set { _CWR = value; }
        }

        public MBit NS
        {
            get { return _NS; }
            set { _NS = value; }
        }

        public MBit Reserved
        {
            get { return _Reserved; }
            set { _Reserved = value; }
        }

        public MBit DataOffset
        {
            get { return _DataOffset; }
            set { _DataOffset = value; }
        }

        public MBit AcknowledgementNumber
        {
            get { return _AcknowledgementNumber; }
            set { _AcknowledgementNumber = value; }
        }

        public MBit SequenceNumber
        {
            get { return _SequenceNumber; }
            set { _SequenceNumber.SetValue(value); }
        }

        public MBit DestinationPort
        {
            get { return _DestinationPort; }
            set { _DestinationPort.SetValue(value); }
        }

        public MBit SourcePort
        {
            get { return _SourcePort; }
            set { _SourcePort.SetValue(value); }
        }


        public override Header this[string name]
        {
            get
            {
                if(name.ToLower() == "tcp")
                {
                    return this;
                }
                return null;
            }
        }

        public override bool IsCompactible(Header d)
        {
            return true;
        }
    }
}
