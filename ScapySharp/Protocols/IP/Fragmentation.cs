﻿using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.IP
{
    public class Fragmentation : MBit
    {

        private Fragmentation(int size, int value) : base(size, value)
        {

        }
    
        public static Fragmentation DF = new Fragmentation(3, 2);
        public static Fragmentation MF = new Fragmentation(3, 1);

    }
}
