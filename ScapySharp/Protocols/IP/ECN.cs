﻿using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.IP
{
    public class ECN : MBit
    {
        private ECN(MBit type) : base(type)
        {

        }

        private static ECN _NonCapableTransport = new ECN(new MBit(2, 0));
        private static ECN _ECT0 = new ECN(new MBit(2, 2));
        private static ECN _ECT1 = new ECN(new MBit(2, 1));
        private static ECN _CE = new ECN(new MBit(2, 3));


        public static ECN NonCapableTransport()
        {
            return new ECN(_NonCapableTransport);
        }

        public static ECN ECT0()
        {
            return new ECN(_ECT0);
        }

        public static ECN ECT1()
        {
            return new ECN(_ECT1);
        }

        public static ECN CE()
        {
            return new ECN(_CE);
        }
    }
}
