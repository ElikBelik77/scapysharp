﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.IP
{
    public static class IPProtocols
    {
        public static int ICMP = 1;
        public static int TCP = 6;
        public static int UDP = 17;
    }
}
