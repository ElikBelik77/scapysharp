﻿using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.IP
{
    /// <summary>
    /// DiffServ Code Point implementation
    /// </summary>
    /// <seealso cref="ScapySharp.Utillities.MBit" />
    public class DSCP : MBit
    {
        private MBit _Type;

        public MBit CS
        {
            get
            {
                return _Type[0, 3];
            }
        }

        public MBit AF
        {
            get
            {
                return _Type[3, 6];
            }
        }


        private DSCP(MBit type) : base(type.Values.Length, (int)type)
        {
            _Type = type;
        }

        public static DSCP CreateDS(int classPriority, int assuredForwarding)
        {
            DSCP DSCP = new DSCP(new MBit(3, classPriority).PostConcat(new MBit(2, assuredForwarding)).PostConcat(new MBit(1,0)));
            return DSCP;
        }

        public static DSCP EF = CreateDS(5, 3);
        public static DSCP BE = CreateDS(0, 0);


    }
}
