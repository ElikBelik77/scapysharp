﻿using ScapySharp.Utillities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScapySharp.Protocols.IP
{
    public class IPv4Header : Header
    {
        #region Member Variables
        private MBit _Version;
        private MBit _IHL;
        private MBit _TOS;
        private MBit _TotalLength;
        private MBit _Identification;
        private MBit _Flags;
        private MBit _FragmentOffset;
        private MBit _TimeToLive;

       
        private MBit _Protocol;
        private MBit _HeaderChecksum;
        private MBit _SourceIPAddress;
        private MBit _DestinationIPAddress;
        private MBit _Options;
        #endregion

        #region Constructors
        public IPv4Header() : base()
        {
            _Version = new MBit(4, 4); _Bits.Add(_Version);
            _IHL = new MBit(4,5); _Bits.Add(_IHL);
            _TOS = new MBit(8); _Bits.Add(_TOS);
            _TotalLength = new MBit(16); _Bits.Add(_TotalLength);
            _Identification = new MBit(16); _Bits.Add(_Identification);
            _Flags = new MBit(3); _Bits.Add(_Flags);
            _FragmentOffset = new MBit(13); _Bits.Add(_FragmentOffset);
            _TimeToLive = new MBit(8); _Bits.Add(_TimeToLive);
            _Protocol = new MBit(8); _Bits.Add(_Protocol);
            _HeaderChecksum = new MBit(16); _Bits.Add(_HeaderChecksum);
            _SourceIPAddress = new MBit(32); _Bits.Add(_SourceIPAddress);
            _DestinationIPAddress = new MBit(32); _Bits.Add(_DestinationIPAddress);
            _Options = new MBit(0); _Bits.Add(_Options);
        }
        #endregion

        #region Properties
        public int HeaderLength
        {
            get { return 5 + Options.Values.Length / 8; }
        }

        public MBit IHL
        {
            get { return _IHL; }
            set { _IHL.SetValue(value); }
        }
        public MBit Options
        {
            get { return _Options; }
            set { _Options.SetValue(value); }
        }

        public MBit DestinationIPAddress
        {
            get { return _DestinationIPAddress; }
            set { _DestinationIPAddress.SetValue(value); }
        }

        public MBit SourceIPAddress
        {
            get { return _SourceIPAddress; }
            set { _SourceIPAddress.SetValue(value); }
        }

        public MBit HeaderChecksum
        {
            get { return _HeaderChecksum; }
            set { _HeaderChecksum.SetValue(value); }
        }

        public MBit Protocol
        {
            get { return _Protocol; }
            set { _Protocol.SetValue(value); }
        }

        public MBit Identification
        {
            get { return _Identification; }
            set { _Identification.SetValue(value); }
        }

        public MBit TotalLength
        {
            get { return _TotalLength; }
            set { _TotalLength.SetValue(value); }
        }

        public MBit TimeToLive
        {
            get { return _TimeToLive; }
            set { _TimeToLive.SetValue(value); }
        }

        public MBit FragmentOffset
        {
            get { return _FragmentOffset; }
            set { _FragmentOffset.SetValue(value); }
        }

        public MBit Flags
        {
            get { return _Flags; }
            set { _Flags.SetValue(value); }
        }

        public MBit TOS
        {
            get { return _TOS; }
            set { _TOS.SetValue(value); }
        }

        public MBit Version
        {
            get { return _Version; }
        }

        public override Header this[string name]
        {
            get
            {
                if (name.ToLower() == "ipv4")
                {
                    return this;
                }
                return null;
            }
        }


        #endregion

        protected override byte[] GetHeaderData()
        {

            MBit result = new MBit(0, 0);
            foreach(MBit b in _Bits)
            {
                result= result.PostConcat(b);
            }
            return result.GetBytes();
        }

        public override bool IsCompactible(Header d)
        {
            return true;
        }

        public void CalculateChecksum()
        {
            int sum = 0;
            List<byte> bytes = new List<byte>();
            foreach(MBit bit in this._Bits)
            {
                if (bit == _HeaderChecksum)
                    continue;
                bytes.AddRange(bit.GetBytes());
            }
            bytes = ByteCompressor.Compress(bytes.ToArray()).ToList();
            bool skip = false;
            foreach (Tuple<byte, byte> c in bytes.Zip(bytes.Skip(1), (a, b) => new Tuple<byte, byte>(a, b)))
            {
                if (!skip)
                {
                    skip = true;
                }
                else
                {
                    skip = false;
                    continue;
                }
                sum += (c.Item1 << 8 ) + c.Item2;
            }
            byte[] intBytes = BitConverter.GetBytes(sum);
            int fifthByte = (intBytes[2] << 4 ) >> 4;
            sum += fifthByte;
            sum %= (int)Math.Pow(2, 16);
            //Array.Reverse(sum);
            //int carry = (sum % (int)Math.Pow(2, 23)) / (int)Math.Pow(2, 15);
            //sum %= (int)Math.Pow(2, 15);
            //int checksum = carry + sum;
            this.HeaderChecksum = new MBit(16,sum).BitwiseNot();
        }
    }
}
